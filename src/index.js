import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { createStore } from 'redux';

import App from './App';

import channelReducers from './reducers/channelsReducers'

import './index.css';

const store = createStore(
  channelReducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

const Root = () => (
  <Provider store={store}>
    <App />
  </Provider>
)


ReactDOM.render(<Root />, document.getElementById('root'));
