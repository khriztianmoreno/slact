import { ADD_CHANNEL, LOAD_CHANNELS } from './../types/channelsType';

const initialState = {
  channels: ['general'],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_CHANNEL:
      return {
        ...state,
        channels: state.channels.concat(action.channel)
      }
      break;
    case LOAD_CHANNELS:
      return state;
      break;
    default:
      return state;
      break;
  };
};
