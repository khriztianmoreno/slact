import React from 'react';

const Messages = () => (
  <div id="message-list">
    <div className="time-divide">
      <span className="date" />
    </div>
    <div className="message">
      <a href="https://twitter.com/makeitrealcamp/" target="_blank">
        <img src="https://pbs.twimg.com/profile_images/618535205649911808/r0u3iZ2z_normal.png" className="message_profile-pic" />
      </a>
      <a href="https://twitter.com/makeitrealcamp/" target="_blank" className="message_username">makeitrealcamp</a>
      <span className="message_timestamp">1:31 PM</span>
      <span className="message_content">Hi there! </span>😘
    </div>

    <div className="message">
      <a href="https://twitter.com/makeitrealcamp/" target="_blank">
        <img src="https://pbs.twimg.com/profile_images/618535205649911808/r0u3iZ2z_normal.png" className="message_profile-pic" />
      </a>
      <a href="https://twitter.com/makeitrealcamp/" target="_blank" className="message_username">makeitrealcamp</a>
      <span className="message_timestamp">1:31 PM</span>
      <span className="message_content">Welcome to your chat app</span>
    </div>
  </div>
);

export default Messages;
