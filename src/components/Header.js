import React from 'react';

const Header = () => (
  <div className="header">
    <div className="team-menu">
      <img className="connection_icon" src="./connect.png" />
      anonymous
    </div>
    <div className="channel-menu">
      <span className="channel-menu_name">
        <span className="channel-menu_prefix">#</span>
        general
      </span>
    </div>
  </div>
);

export default Header;
