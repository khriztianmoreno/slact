import React from 'react';

const Footer = () => (
  <div className="footer">
    <div className="user-menu">
      <p className="disclaimer">This demo is not created by, affiliated with, or supported by Slack Technologies, Inc.</p>
    </div>
    <div className="input-box">
      <input type="text" className="input-box_text" />
    </div>
  </div>
);

export default Footer;
