import React, { Component } from 'react';
import Modal from 'react-modal';
import { connect } from 'react-redux';

import { loadChannels, createChannel } from './../actions/channelsActions'

const modalStyle = {
  content : {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
};

class Channels extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalIsOpen: false,
      channelName: ''
    }

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.joinChannel = this.joinChannel.bind(this);
  }

  openModal() {
    this.setState({modalIsOpen: true});
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  joinChannel() {
    const { channelName, channels } = this.state;

    const name = channelName.toLowerCase().split(' ').join('-');
    this.props.createData(name)
    this.setState({ channels });

    this.closeModal();
  }

  componentDidMount() {
    this.props.loadData()
  }

  render() {
    const { channels } = this.props;
    return (
      <div className="listings">
        <div className="listings_channels">
          <span
            className="add_icon"
            onClick={this.openModal}
          >
            +
          </span>
          <h2 className="listings_header">Channels</h2>
          <ul className="channel_list">
            {
              channels && channels.length > 0 && channels.map((channel, index) => (
                <li
                  className={channels.length === index + 1 ? "channel active" : "channel "}
                  key={index}>
                  <a className="channel_name">
                    <span className="unread">0</span>
                    <span>
                      <span className="prefix">#</span>
                      {channel}
                    </span>
                  </a>
                </li>
              ))
            }
          </ul>
        </div>
        <div className="listings_direct-messages"></div>
        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          style={modalStyle}>
          <h2 className="text-center">Enter a channel to join</h2>
          <div>
            #
            <input
              id="new-channel-name"
              type="text"
              autoFocus
              onChange={(ev) => {
                this.setState({channelName: ev.target.value});
              }}
            />
            <button
              className="btn"
              onClick={this.joinChannel}
            >
              Join
            </button>
          </div>
        </Modal>

      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    channels: state.channels
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadData: loadChannels,
    createData(name) {
      dispatch(createChannel(name))
    }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Channels);
