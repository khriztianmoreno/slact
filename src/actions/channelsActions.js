import { ADD_CHANNEL, LOAD_CHANNELS } from './../types/channelsType';

export function loadChannels() {
  return {
    type: LOAD_CHANNELS,
  };
}

export function createChannel(channel) {
  return {
    type: ADD_CHANNEL,
    channel,
  };
}
