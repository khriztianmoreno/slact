import React, { Component } from 'react';

import Header from './components/Header';
import Footer from './components/Footer';
import Channels from './components/Channels';
import Messages from './components/Messages';


class App extends Component {
  createChannel (name) {
    console.log('Channel: ', name);
  }

  render() {
    return (
      <div className="app">
        <Header />
        <div className="main">
          <Channels />
          <div className="message-history">
            <Messages />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
